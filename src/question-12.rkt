; --------------------------------
; Question 12
; --------------------------------
; fn main() {
;   let x = vec![1, 2, 3];
;   let y: i32 = x.iter().fold(0, |acc, i| acc + i );
; }

#lang racket

(module test racket 
  (require "assert.rkt")
  ; Add code below
  ; ------------------
  (define x (list 1 2 3))
  (define (sum l)
    (if (empty? l) 0
        (+ (first l) (sum (rest l)))))
  (define y (sum x))
  (write y)
  ; ------------------
  ; Add code above
  (assert y 6))